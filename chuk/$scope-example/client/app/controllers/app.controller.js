(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http", "$interval", "$scope"];

    function MyAppCtrl($http, $interval, $scope) {
        var vm = this; // vm
        vm.counter = 0;
        var id = -1;

        vm.init = function () {
            if (id == -1) {
                id = setInterval(function () {
                    // id = $interval(function () {
                    $scope.$apply(function () {
                        vm.counter++;
                        console.log(vm.counter);
                    })
                }, 1000);
            } else {
                clearInterval(id);
                id = -1;
            }
        };

        vm.init();
    }

})();