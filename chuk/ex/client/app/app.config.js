(function () {
    "use strict";
    angular.module("CustomerApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/views/main.html",
                controller: "MainCtrl as ctrl"
            })
            .state("details", {
                url: "/details/:term",
                templateUrl: "/views/details.html",
                controller: "DetailsCtrl as ctrl"
            })

        $urlRouterProvider.otherwise("main");
    }





})();