(function () {
    angular.module("CustomerApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q"];

    function MyAppService($http, $q) {
        var service = this;

        //expose the following services
        service.getImages = getImages;
        service.getUsers = getUsers;
        service.getUserById = getUserById;
        service.updateUser = updateUser;
        service.findCustomerByName = findCustomerByName;
        service.findPurchaseOrderByCustomerId = findPurchaseOrderByCustomerId;


        function findPurchaseOrderByCustomerId(id) {
            var defer = $q.defer();

            $http.get("/api/customer/" + id +"/purchaseorder").then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function findCustomerByName(name) {
            var defer = $q.defer();

            $http.post("/api/customer", { term: name })
                .then(function (result) {
                    // var customer = result.data;
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function updateUser(user) {
            var defer = $q.defer();
            const id = user.regid;
            $http.post("/user/" + id, { user: user })
                .then(function (result) {
                    // var customer = result.data;
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getUserById(id) {
            var defer = $q.defer();

            $http.get("/user/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function getUsers() {
            var defer = $q.defer();

            $http.get("/users").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getImages(tag) {

            var defer = $q.defer();

            $http.get("https://api.giphy.com/v1/gifs/search", {
                params: {
                    api_key: "ac419443623246189d35e51eab60c1d9",
                    q: tag,
                    limit: 5,
                    offset: 0
                }
            }).then(function (result) {
                console.log(result.data.data[0]);
                var data = result.data.data;
                var images = []
                for (var i in data) {
                    images.push(data[i].images.fixed_height_small.url);
                    // images.push(data[i].images.downsized_medium.url);      
                }
                var tag = self.tag;
                defer.resolve(images);
            }, function (err) {
                deferred.reject(err);
            });

            return defer.promise;
        }

    }

})();