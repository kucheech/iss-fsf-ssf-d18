(function () {
    "use strict";
    angular.module("CustomerApp").controller("MainCtrl", MainCtrl);

    MainCtrl.$inject = ["MyAppService", "$state"];

    function MainCtrl(MyAppService, $state) {
        // console.log("BooksCtrl");
        var vm = this; // vm
        vm.term = "";

        vm.search = function () {
            // console.log(id);
            $state.go("details", { term: vm.term });
        }

        vm.init = function () {
            // MyAppService.getUsers()
            //     .then(function (result) {
            //         // console.log(result);
            //         self.users = result;
            //     }).catch(function (err) {
            //         console.log(err);
            //     });

        }

        vm.init();


    }

})();