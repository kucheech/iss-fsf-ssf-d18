(function () {
    "use strict";
    angular.module("CustomerApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http", "$log"];

    function MyAppCtrl($http) {
        var self = this; // vm

        self.initForm = function () {
            $http.get("/hello")
                .then(function (result) {
                    console.log(result);
                    self.reply = result.data;
                }).catch(function (e) {
                    console.log(e);
                });
        };

        self.initForm();
    }

})();