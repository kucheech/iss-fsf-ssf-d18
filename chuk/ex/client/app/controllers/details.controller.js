(function () {
    "use strict";
    angular.module("CustomerApp").controller("DetailsCtrl", DetailsCtrl);

    DetailsCtrl.$inject = ["MyAppService", "$state"];

    function DetailsCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.customer = null;
        vm.term = "";
        vm.orders = [];
        vm.customerFound = false;
        vm.showTable = false;
        vm.showWarning = false;

        vm.goBack = function () {
            $state.go("main");
        }

        vm.showPurchaseOrders = function () {

            vm.showTable = false;

            MyAppService.findPurchaseOrderByCustomerId(vm.customer.CUSTOMER_ID)
                .then(function (result) {
                    // console.log(result);
                    vm.orders = result;
                    if(vm.orders.length > 0) {
                        vm.showTable = true;
                    } else {
                        vm.showWarning = true;
                    }
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init = function () {
            // console.log($state.params.id);
            vm.term = $state.params.term;
            // console.log("term = " + vm.term);
            // vm.showResults = false;
            vm.customerFound = false;
            MyAppService.findCustomerByName(vm.term)
                .then(function (result) {
                    // console.log(result);
                    vm.customer = result.data || null;
                    if (vm.customer != null) {
                        vm.customerFound = true;
                    }
                }).catch(function (err) {
                    console.log(err);
                });

        }

        vm.init();


    }

})();