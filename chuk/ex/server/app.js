const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

const sessionStore = new MySQLStore({
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'derby_sample'
});

// const sessionConfig = session({
//     secret: "fsf17r3",
//     resave: false,
//     saveUninitialized: true
// })

const sessionConfig = session({
    secret: "fsf17r3",
    resave: false,
    saveUninitialized: true,
    store: sessionStore,
    cookie: {
        maxAge: 30 * 60 * 1000
    }
})

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(sessionConfig);

app.use(function (req, res, next) {
    if (!req.session.queryCache) {
        console.log("new");
        req.session.queryCache = {};
    } else {
        // console.log(req.session.queryCache);
    }

    next();
});

const NODE_PORT = process.env.PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));


var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'derby_sample'
});


// //using q
const mkQuery = function (sql, pool) {

    const sqlQuery = function () {
        const defer = q.defer();

        var sqlParams = [];
        for (i in arguments) {
            sqlParams.push(arguments[i]);
        }

        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, sqlParams, function (err, result) {
                if (err) {
                    defer.reject(err);
                    console.log(err);
                } else {
                    // console.log(result);
                    defer.resolve(result);
                }
                conn.release();
            });
        });

        return defer.promise;
    }

    return sqlQuery;
};

const SELECT_ALL_USERS = "select * from users";
const SELECT_USER_BY_ID = "select * from users where regid = ? limit 1";
const UPDATE_USER_BY_ID = "update users set name = ?, email = ?, phone = ?, dob = ? where regid = ?";

const SELECT_CUSTOMER_BY_NAME = "select * from customer where name like ?";
const SELECT_PURCHASE_ORDER_BY_CUSTOMER_ID = "select * from purchase_order where customer_id = ?";


const getAllUsers = mkQuery(SELECT_ALL_USERS, pool);
const getUserById = mkQuery(SELECT_USER_BY_ID, pool);
const updateUserById = mkQuery(UPDATE_USER_BY_ID, pool);

const findCustomerByName = mkQuery(SELECT_CUSTOMER_BY_NAME, pool);
const findPurchaseOrderByCustomerId = mkQuery(SELECT_PURCHASE_ORDER_BY_CUSTOMER_ID, pool);


app.post("/api/customer", function (req, res) {
    const term = "%" + req.body.term + "%";
    findCustomerByName(term).then(function (result) {
        if (result.length > 0) {
            var customer = result[0];
            res.status(200).json(customer);
        } else {
            res.status(404).send("Customer not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.post("/api/po", function (req, res) {
    const term = "%" + req.body.term + "%";

    console.log(req.session.queryCache);
    if (req.session.queryCache[req.body.term]) {
        console.log("Cache hit");
    }

    findCustomerByName(term).then(function (result) {
        return result.length > 0 ? result[0] : q.reject("Customer not found");
    }).then(function (customer) {
        findPurchaseOrderByCustomerId(customer.CUSTOMER_ID).then(function (result) {
            if (result.length > 0) {
                customer.purchaseorders = result;

                req.session.queryCache[req.body.term] = {
                    result: customer,
                    timestamp: (new Date()).getTime()
                };

                res.status(200).json(customer);

                // req.session.save();
                // console.log(req.session.queryCache);
            } else {
                res.status(404).send("Purchase order not found");
            }
        });
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/api/customer/:id/purchaseorder", function (req, res) {
    const id = parseInt(req.params.id);
    findPurchaseOrderByCustomerId(id).then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("Purchase order not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});


function formatDate(date1) {
    return date1.getFullYear() + '-' +
        (date1.getMonth() < 9 ? '0' : "") + (date1.getMonth() + 1) + '-' +
        (date1.getDate() < 10 ? '0' : "") + date1.getDate();
}

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});


app.post("/user/:id", function (req, res) {
    const id = req.params.id;
    const user = req.body.user;
    updateUserById(user.name, user.email, user.phone, formatDate(new Date(user.dob)), id)
        .then(function (result) {
            console.log(result);
            // res.status(404).type("text/plain").send("Customer not found");
            res.status(200).end();
        }).catch(function (err) {
            handleError(err, res);
        });
});



app.get("/users", function (req, res) {

    getAllUsers().then(function (result) {
        if (result.length > 0) {
            res.status(200).json(result);
        } else {
            res.status(404).send("No users");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

app.get("/user/:id", function (req, res) {
    const id = req.params.id;
    getUserById(id).then(function (result) {
        if (result.length > 0) {
            var book = result[0];
            res.status(200).json(book);
        } else {
            res.status(404).send("User not found");
        }
    }).catch(function (err) {
        handleError(err, res);
    });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app