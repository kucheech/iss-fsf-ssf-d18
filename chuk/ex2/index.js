const numbers = [1, 2, 3, 4, 5];

numbers.forEach(function (i) {
    console.log(i);
});

var double = numbers.map(function (i) {
    return 2 * i;
});

console.log("double: " + double);

var greaterThan5 = double.filter(function (i) {
    return i > 5;
})

console.log(greaterThan5);

console.log("Chaining")
numbers
    .map(function (i) {
        return i * 3;
    }).filter(function (i) {
        return i > 5;
    }).forEach(function (i) {
        console.log(i);
    });

numbers
    .map(function (i) {
        return [i, i * 3];
    }).forEach(function (i) {
        console.log(i);
    });